﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace fooBet.Data.Migrations
{
    /// <inheritdoc />
    public partial class Initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "countries",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    title = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_countries", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "line_entities",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    title = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_line_entities", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "market_types",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    has_spread = table.Column<bool>(type: "boolean", nullable: false),
                    short_title = table.Column<string>(type: "text", nullable: true),
                    possible_outcomes = table.Column<List<string>>(type: "text[]", nullable: true),
                    title = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_market_types", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "notes",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    message = table.Column<string>(type: "text", nullable: true),
                    title = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_notes", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "sports",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    title = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_sports", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "game_periods",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    sport_id = table.Column<int>(type: "integer", nullable: true),
                    title = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_game_periods", x => x.id);
                    table.ForeignKey(
                        name: "fk_game_periods_sports_sport_id",
                        column: x => x.sport_id,
                        principalTable: "sports",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "leagues",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    sport_id = table.Column<int>(type: "integer", nullable: true),
                    gender = table.Column<int>(type: "integer", nullable: false),
                    title = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_leagues", x => x.id);
                    table.ForeignKey(
                        name: "fk_leagues_sports_sport_id",
                        column: x => x.sport_id,
                        principalTable: "sports",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "source_sports",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    sport_id = table.Column<int>(type: "integer", nullable: false),
                    title = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_source_sports", x => x.id);
                    table.ForeignKey(
                        name: "fk_source_sports_sports_sport_id",
                        column: x => x.sport_id,
                        principalTable: "sports",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "teams",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    sport_id = table.Column<int>(type: "integer", nullable: true),
                    country_id = table.Column<int>(type: "integer", nullable: true),
                    gender = table.Column<int>(type: "integer", nullable: false),
                    title = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_teams", x => x.id);
                    table.ForeignKey(
                        name: "fk_teams_countries_country_id",
                        column: x => x.country_id,
                        principalTable: "countries",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_teams_sports_sport_id",
                        column: x => x.sport_id,
                        principalTable: "sports",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "source_leagues",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    league_id = table.Column<int>(type: "integer", nullable: false),
                    title = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_source_leagues", x => x.id);
                    table.ForeignKey(
                        name: "fk_source_leagues_leagues_league_id",
                        column: x => x.league_id,
                        principalTable: "leagues",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "events",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    league_id = table.Column<int>(type: "integer", nullable: true),
                    home_team_id = table.Column<int>(type: "integer", nullable: true),
                    away_team_id = table.Column<int>(type: "integer", nullable: true),
                    start_date = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    title = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_events", x => x.id);
                    table.ForeignKey(
                        name: "fk_events_leagues_league_id",
                        column: x => x.league_id,
                        principalTable: "leagues",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_events_teams_away_team_id",
                        column: x => x.away_team_id,
                        principalTable: "teams",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_events_teams_home_team_id",
                        column: x => x.home_team_id,
                        principalTable: "teams",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "source_teams",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    team_id = table.Column<int>(type: "integer", nullable: false),
                    title = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_source_teams", x => x.id);
                    table.ForeignKey(
                        name: "fk_source_teams_teams_team_id",
                        column: x => x.team_id,
                        principalTable: "teams",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "markets",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    event_id = table.Column<int>(type: "integer", nullable: true),
                    game_period_id = table.Column<int>(type: "integer", nullable: true),
                    line_entity_id = table.Column<int>(type: "integer", nullable: true),
                    market_type_id = table.Column<int>(type: "integer", nullable: true),
                    options_index = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_markets", x => x.id);
                    table.ForeignKey(
                        name: "fk_markets_events_event_id",
                        column: x => x.event_id,
                        principalTable: "events",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_markets_game_periods_game_period_id",
                        column: x => x.game_period_id,
                        principalTable: "game_periods",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_markets_line_entities_line_entity_id",
                        column: x => x.line_entity_id,
                        principalTable: "line_entities",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_markets_market_types_market_type_id",
                        column: x => x.market_type_id,
                        principalTable: "market_types",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "results",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    sport_id = table.Column<int>(type: "integer", nullable: true),
                    event_id = table.Column<int>(type: "integer", nullable: true),
                    line_entities_id = table.Column<int>(type: "integer", nullable: true),
                    game_periods_id = table.Column<int>(type: "integer", nullable: true),
                    home = table.Column<int>(type: "integer", nullable: false),
                    away = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_results", x => x.id);
                    table.ForeignKey(
                        name: "fk_results_events_event_id",
                        column: x => x.event_id,
                        principalTable: "events",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_results_game_periods_game_periods_id",
                        column: x => x.game_periods_id,
                        principalTable: "game_periods",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_results_line_entities_line_entities_id",
                        column: x => x.line_entities_id,
                        principalTable: "line_entities",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_results_sports_sport_id",
                        column: x => x.sport_id,
                        principalTable: "sports",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "odds",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    market_id = table.Column<int>(type: "integer", nullable: true),
                    outcome_key = table.Column<string>(type: "text", nullable: true),
                    spread = table.Column<double>(type: "double precision", nullable: false),
                    value = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_odds", x => x.id);
                    table.ForeignKey(
                        name: "fk_odds_markets_market_id",
                        column: x => x.market_id,
                        principalTable: "markets",
                        principalColumn: "id");
                });

            migrationBuilder.CreateIndex(
                name: "ix_events_away_team_id",
                table: "events",
                column: "away_team_id");

            migrationBuilder.CreateIndex(
                name: "ix_events_home_team_id",
                table: "events",
                column: "home_team_id");

            migrationBuilder.CreateIndex(
                name: "ix_events_league_id",
                table: "events",
                column: "league_id");

            migrationBuilder.CreateIndex(
                name: "ix_game_periods_sport_id",
                table: "game_periods",
                column: "sport_id");

            migrationBuilder.CreateIndex(
                name: "ix_leagues_sport_id",
                table: "leagues",
                column: "sport_id");

            migrationBuilder.CreateIndex(
                name: "ix_markets_event_id",
                table: "markets",
                column: "event_id");

            migrationBuilder.CreateIndex(
                name: "ix_markets_game_period_id",
                table: "markets",
                column: "game_period_id");

            migrationBuilder.CreateIndex(
                name: "ix_markets_line_entity_id",
                table: "markets",
                column: "line_entity_id");

            migrationBuilder.CreateIndex(
                name: "ix_markets_market_type_id",
                table: "markets",
                column: "market_type_id");

            migrationBuilder.CreateIndex(
                name: "ix_odds_market_id",
                table: "odds",
                column: "market_id");

            migrationBuilder.CreateIndex(
                name: "ix_results_event_id",
                table: "results",
                column: "event_id");

            migrationBuilder.CreateIndex(
                name: "ix_results_game_periods_id",
                table: "results",
                column: "game_periods_id");

            migrationBuilder.CreateIndex(
                name: "ix_results_line_entities_id",
                table: "results",
                column: "line_entities_id");

            migrationBuilder.CreateIndex(
                name: "ix_results_sport_id",
                table: "results",
                column: "sport_id");

            migrationBuilder.CreateIndex(
                name: "ix_source_leagues_league_id",
                table: "source_leagues",
                column: "league_id");

            migrationBuilder.CreateIndex(
                name: "ix_source_sports_sport_id",
                table: "source_sports",
                column: "sport_id");

            migrationBuilder.CreateIndex(
                name: "ix_source_teams_team_id",
                table: "source_teams",
                column: "team_id");

            migrationBuilder.CreateIndex(
                name: "ix_teams_country_id",
                table: "teams",
                column: "country_id");

            migrationBuilder.CreateIndex(
                name: "ix_teams_sport_id",
                table: "teams",
                column: "sport_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "notes");

            migrationBuilder.DropTable(
                name: "odds");

            migrationBuilder.DropTable(
                name: "results");

            migrationBuilder.DropTable(
                name: "source_leagues");

            migrationBuilder.DropTable(
                name: "source_sports");

            migrationBuilder.DropTable(
                name: "source_teams");

            migrationBuilder.DropTable(
                name: "markets");

            migrationBuilder.DropTable(
                name: "events");

            migrationBuilder.DropTable(
                name: "game_periods");

            migrationBuilder.DropTable(
                name: "line_entities");

            migrationBuilder.DropTable(
                name: "market_types");

            migrationBuilder.DropTable(
                name: "leagues");

            migrationBuilder.DropTable(
                name: "teams");

            migrationBuilder.DropTable(
                name: "countries");

            migrationBuilder.DropTable(
                name: "sports");
        }
    }
}
