﻿using AutoMapper;
using fooBet.Core.Contracts.Repos;
using fooBet.Core.Models.Abstract;
using fooBet.Data.Entities.Abstract;
using Microsoft.EntityFrameworkCore;

namespace fooBet.Data.Repos
{
    public abstract class BaseRepository<TModel, TEntity> : IBaseRepository<TModel>
       where TModel : BaseTitleModel
       where TEntity : BaseTitleEntity
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        protected BaseRepository(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public virtual async Task<TModel> AddAsync(TModel model)
        {
            var entity = _mapper.Map<TEntity>(model);
            _context.Entry(entity).State = EntityState.Modified;
            await _context.Set<TEntity>().AddAsync(entity);
            await _context.SaveChangesAsync();

            return _mapper.Map<TModel>(entity);
        }

        public virtual async Task<IEnumerable<TModel>> GetAllAsync()
        {
            var res = await _context.Set<TEntity>()
                                    .AsQueryable()
                                    .AsNoTracking()
                                    .ToListAsync();

            return _mapper.Map<IEnumerable<TModel>>(res);
        }

        public virtual async Task<TModel> GetByIdAsync(int id)
        {
            var entity = await _context.Set<TEntity>()
                                       .AsNoTracking()
                                       .FirstOrDefaultAsync(x => x.Id == id);

            return _mapper.Map<TModel>(entity);
        }

        public virtual async Task<TModel> GetByTitleAsync(string title)
        {
            var entity = await _context.Set<TEntity>()
                                       .AsNoTracking()
                                       .FirstOrDefaultAsync(x => x.Title == title);

            return _mapper.Map<TModel>(entity);
        }

        public virtual async Task<TModel> UpdateAsync(TModel model)
        {
            var entity = _mapper.Map<TEntity>(model);

            _context.Set<TEntity>().Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return _mapper.Map<TModel>(entity);
        }

        public virtual async Task<bool> DeleteAsync(int id)
        {
            var entity = await _context.Set<TEntity>()
                                       .FirstOrDefaultAsync(x => x.Id == id);
           
            if (entity == null)
            {
                return false;
            }

            _context.Set<TEntity>().Remove(entity);

            return await _context.SaveChangesAsync() > default(int);
        }

    }
}
