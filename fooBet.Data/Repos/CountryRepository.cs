﻿using AutoMapper;
using fooBet.Core.Contracts.Repos;
using fooBet.Core.Models;
using fooBet.Data.Entities;

namespace fooBet.Data.Repos
{
    public class CountryRepository : BaseRepository<Country, CountryEntity>, ICountryRepository
    {
        public CountryRepository(DataContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}
