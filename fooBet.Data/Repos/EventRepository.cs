﻿using AutoMapper;
using fooBet.Core.Contracts.Repos;
using fooBet.Core.Models;
using fooBet.Data.Entities;

namespace fooBet.Data.Repos
{
    public class EventRepository : BaseRepository<Event, EventEntity>, IEventRepository
    {
        public EventRepository(DataContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}
