﻿using AutoMapper;
using fooBet.Core.Contracts.Repos;
using fooBet.Core.Models;
using fooBet.Data.Entities;

namespace fooBet.Data.Repos
{
    public class MarketTypeRepository : BaseRepository<MarketType, MarketTypeEntity>, IMarketTypeRepository
    {
        public MarketTypeRepository(DataContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}
