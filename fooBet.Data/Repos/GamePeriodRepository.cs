﻿using AutoMapper;
using fooBet.Core.Contracts.Repos;
using fooBet.Core.Models;
using fooBet.Data.Entities;

namespace fooBet.Data.Repos
{
    public class GamePeriodRepository : BaseRepository<GamePeriod, GamePeriodEntity>, IGamePeriodRepository
    {
        public GamePeriodRepository(DataContext context, IMapper mapper) : base(context, mapper)
        {
            
        }
    }
}
