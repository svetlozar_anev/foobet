﻿using AutoMapper;
using fooBet.Core.Contracts.Repos;
using fooBet.Core.Models;
using fooBet.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Npgsql.EntityFrameworkCore.PostgreSQL.Query.Expressions.Internal;
using System.Reflection;

namespace fooBet.Data.Repos
{
    public class LeagueRepository : BaseRepository<League, LeagueEntity>, ILeagueRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public LeagueRepository(DataContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public override async Task<League> AddAsync(League league)
        { 
            var entity = _mapper.Map<LeagueEntity>(league);
            _context.Entry(entity).State = EntityState.Modified;

            await _context.Set<LeagueEntity>().AddAsync(entity);

            foreach (var sourceLeague in entity.SourceLeagues)
            {
                await _context.Set<SourceLeagueEntity>().AddAsync(sourceLeague);
            }

            await _context.SaveChangesAsync();

            return _mapper.Map<League>(entity);
        }

        public override async Task<League> GetByTitleAsync(string title)
        {
            var entity = await _context.Leagues
                                       .Include(s => s.Sport)
                                       //   .Include(g => g.Gender)
                                       // https://itnext.io/how-to-use-enums-when-using-entity-framework-core-with-c-bb634692a4b0
                                       .AsNoTracking()
                                       .FirstOrDefaultAsync(x => x.Title == title);

            return _mapper.Map<League>(entity);
        }
    }
}
