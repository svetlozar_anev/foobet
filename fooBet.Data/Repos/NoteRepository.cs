﻿using AutoMapper;
using fooBet.Core.Contracts.Repos;
using fooBet.Core.Models;
using fooBet.Data.Entities;

namespace fooBet.Data.Repos
{
    public class NoteRepository : BaseRepository<Note, NoteEntity>, INoteRepository
    {
        public NoteRepository(DataContext context, IMapper mapper) : base(context, mapper)
        { 
        }
    }
}
