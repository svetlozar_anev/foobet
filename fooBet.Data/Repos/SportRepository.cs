﻿using AutoMapper;
using fooBet.Core.Contracts.Repos;
using fooBet.Core.Models;

using fooBet.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace fooBet.Data.Repos
{
    public class SportRepository : BaseRepository<Sport, SportEntity>, ISportRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        public SportRepository(DataContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public override async Task<Sport> AddAsync(Sport sport)
        { 
            var entity = _mapper.Map<SportEntity>(sport);
            _context.Entry(entity).State = EntityState.Modified;

            await _context.Set<SportEntity>().AddAsync(entity);

            foreach (var sourceSport in entity.SourceSports)
            {
                await _context.Set<SourceSportEntity>().AddAsync(sourceSport);
            }

            await _context.SaveChangesAsync();

            return _mapper.Map<Sport>(entity);
        }
    }
}
