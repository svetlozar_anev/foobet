﻿using AutoMapper;
using fooBet.Core.Contracts.Repos;
using fooBet.Core.Models;
using fooBet.Data.Entities;

namespace fooBet.Data.Repos
{
    public class LineEntityRepository : BaseRepository<LineEntity, LineEntityEntity>, ILineEntityRepository
    {
        public LineEntityRepository(DataContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}
