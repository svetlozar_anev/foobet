﻿using AutoMapper;
using fooBet.Core.Contracts.Repos;
using fooBet.Core.Models;
using fooBet.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace fooBet.Data.Repos
{
    public class TeamRepository : BaseRepository<Team, TeamEntity>, ITeamRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public TeamRepository(DataContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public override async Task<Team> AddAsync(Team team)
        {
            var entity = _mapper.Map<TeamEntity>(team);
            _context.Entry(entity).State = EntityState.Modified;
            
            await _context.Set<TeamEntity>().AddAsync(entity);

            foreach (var sourceTeam in entity.SourceTeams)
            {
                await _context.Set<SourceTeamEntity>().AddAsync(sourceTeam);
            }
            await _context.SaveChangesAsync();

            return _mapper.Map<Team>(entity);
        }

        public override async Task<Team> GetByTitleAsync(string title) 
        {
            var entity = await _context.Teams
                                       .Include(x => x.Sport)
                                       .Include(x => x.Country)
                                     //  .Include(x => x.Gender)

                                       .FirstOrDefaultAsync(x => x.Title == title);

            return _mapper.Map<Team>(entity);
        }
    }
}
