﻿using fooBet.Data.Entities.Abstract;

namespace fooBet.Data.Entities
{
    public class NoteEntity : BaseTitleEntity
    {
        public string Message { get; set; }
    }
}
