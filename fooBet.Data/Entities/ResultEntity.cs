﻿using fooBet.Data.Entities.Abstract;

namespace fooBet.Data.Entities
{
    public class ResultEntity : BaseIdEntity
    {
        public SportEntity Sport { get; set; }
        public EventEntity Event { get; set; }
        public LineEntityEntity LineEntities { get; set; }
        public GamePeriodEntity GamePeriods { get; set; }

        public int Home { get; set; }
        public int Away { get; set; }
    }
}
