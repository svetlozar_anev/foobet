﻿using System.ComponentModel.DataAnnotations;

namespace fooBet.Data.Entities.Abstract
{
    public abstract class BaseIdEntity
    {
        [Key]
        public int Id { get; set; }
    }
}
