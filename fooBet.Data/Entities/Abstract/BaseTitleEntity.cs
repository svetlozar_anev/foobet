﻿namespace fooBet.Data.Entities.Abstract
{
    public abstract class BaseTitleEntity : BaseIdEntity
    {
        public string Title { get; set; }
    }
}
