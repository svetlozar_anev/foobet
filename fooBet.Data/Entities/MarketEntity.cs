﻿using fooBet.Data.Entities.Abstract;
using System.ComponentModel.DataAnnotations.Schema;

namespace fooBet.Data.Entities
{
    public class MarketEntity : BaseIdEntity
    {
        public EventEntity Event { get; set; }
        public GamePeriodEntity GamePeriod { get; set; }
        public LineEntityEntity LineEntity { get; set; }
        public MarketTypeEntity MarketType { get; set; }
        public int OptionsIndex { get; set; }
    }
}
