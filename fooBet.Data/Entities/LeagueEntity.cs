﻿using fooBet.Core.Enums;
using fooBet.Data.Entities.Abstract;
using System.ComponentModel.DataAnnotations.Schema;

namespace fooBet.Data.Entities
{
    public class LeagueEntity : BaseTitleEntity
    {
        public SportEntity Sport { get; set; }
        public Gender Gender { get; set; }

        [ForeignKey("LeagueId")]
        public ICollection<SourceLeagueEntity> SourceLeagues { get; set; }
    }
}
