﻿using fooBet.Core.Enums;
using fooBet.Data.Entities.Abstract;
using System.ComponentModel.DataAnnotations.Schema;

namespace fooBet.Data.Entities
{
    public class TeamEntity : BaseTitleEntity
    {
        public SportEntity Sport { get; set; }
        public CountryEntity Country { get; set; }
        public Gender Gender { get; set; }

        [ForeignKey("TeamId")]
        public ICollection<SourceTeamEntity> SourceTeams { get; set; }
    }
}
