﻿using fooBet.Data.Entities.Abstract;
using System.ComponentModel.DataAnnotations.Schema;

namespace fooBet.Data.Entities
{
    public class EventEntity : BaseTitleEntity
    {
        public LeagueEntity League { get; set; }
        public TeamEntity HomeTeam { get; set; }
        public TeamEntity AwayTeam { get; set; }
        public DateTime StartDate { get; set; }
    }
}
