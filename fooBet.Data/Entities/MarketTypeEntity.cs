﻿using fooBet.Data.Entities.Abstract;

namespace fooBet.Data.Entities
{
    public class MarketTypeEntity : BaseTitleEntity
    {
        public bool HasSpread { get; set; }
        public string ShortTitle { get; set; }
        public List<string> PossibleOutcomes { get; set; }
    }
}
