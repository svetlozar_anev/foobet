﻿using fooBet.Data.Entities.Abstract;
using System.ComponentModel.DataAnnotations.Schema;

namespace fooBet.Data.Entities
{
    public class SportEntity : BaseTitleEntity
    {
        [ForeignKey("SportId")]
        public ICollection<SourceSportEntity> SourceSports { get; set; }        
    }
}
