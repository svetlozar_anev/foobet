﻿using fooBet.Data.Entities.Abstract;

namespace fooBet.Data.Entities
{
    public class SourceSportEntity : BaseTitleEntity
    {
        public int SportId { get; set; }
        public SportEntity Sport { get; set; }
    }
}
