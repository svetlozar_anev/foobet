﻿using fooBet.Data.Entities.Abstract;

namespace fooBet.Data.Entities
{
    public class OddEntity : BaseIdEntity
    {
        public MarketEntity Market { get; set; }
        public string OutcomeKey { get; set; } //O/U... !X2
        public double Spread { get; set; }
        public double Value { get; set; }
    }
}
