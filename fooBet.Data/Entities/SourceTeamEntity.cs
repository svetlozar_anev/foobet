﻿using fooBet.Data.Entities.Abstract;

namespace fooBet.Data.Entities
{
    public class SourceTeamEntity : BaseTitleEntity
    {
        public int TeamId { get; set; }
        public TeamEntity Team { get; set; }
    }
}
