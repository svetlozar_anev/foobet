﻿using fooBet.Data.Entities.Abstract;

namespace fooBet.Data.Entities
{
    public class SourceLeagueEntity : BaseTitleEntity
    {
        public int LeagueId { get; set; }
        public LeagueEntity League { get; set; }
    }
}
