﻿using fooBet.Data.Entities.Abstract;

namespace fooBet.Data.Entities
{
    public class GamePeriodEntity : BaseTitleEntity
    {
        public SportEntity Sport { get; set; }
    }
}
