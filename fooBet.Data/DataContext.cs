﻿using fooBet.Core.Enums;
using fooBet.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography.X509Certificates;

namespace fooBet.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options)
            : base(options)
        {
        }

        public virtual DbSet<NoteEntity> Notes { get; set; }
        public virtual DbSet<SportEntity> Sports { get; set; }
        public virtual DbSet<SourceSportEntity> SourceSports { get; set; }
        public virtual DbSet<LeagueEntity> Leagues { get; set; }
        public virtual DbSet<SourceLeagueEntity> SourceLeagues { get; set; }
        public virtual DbSet<GamePeriodEntity> GamePeriods { get; set; }
        public virtual DbSet<MarketTypeEntity> MarketTypes { get; set; }
        public virtual DbSet<OddEntity> Odds { get; set; }
        public virtual DbSet<LineEntityEntity> LineEntities { get; set; }
        public virtual DbSet<EventEntity> Events { get; set; }
        public virtual DbSet<MarketEntity> Markets { get; set; }
        public virtual DbSet<ResultEntity> Results { get; set; }
        public virtual DbSet<CountryEntity> Countries { get; set; }
        public virtual DbSet<TeamEntity> Teams { get; set; }
        public virtual DbSet<SourceTeamEntity> SourceTeams { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    => optionsBuilder.UseNpgsql("Host=localhost:6500;Database=foobet;Username=postgres;Password=password123")
                     .UseSnakeCaseNamingConvention();

    }
}
