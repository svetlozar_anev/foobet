﻿using fooBet.Core.Contracts.Repos;
using fooBet.Data.Repos;
using Microsoft.Extensions.DependencyInjection;

namespace fooBet.Data.Helpers
{
    public static class DependencyExtensions
    {
        public static IServiceCollection RegisterRepositories(this IServiceCollection services)
        {
            services.AddScoped<INoteRepository, NoteRepository>();
            services.AddScoped<ISportRepository, SportRepository>();
            services.AddScoped<ILeagueRepository, LeagueRepository>();
            services.AddScoped<ITeamRepository, TeamRepository>();
            services.AddScoped<ICountryRepository, CountryRepository>();
            services.AddScoped<IEventRepository, EventRepository>();
            services.AddScoped<ILineEntityRepository, LineEntityRepository>();
            services.AddScoped<IGamePeriodRepository, GamePeriodRepository>();
            services.AddScoped<IMarketTypeRepository, MarketTypeRepository>();

            return services;
        }
    }
}
