﻿using AutoMapper;
using fooBet.Core.Models;
using fooBet.Data.Entities;

namespace fooBet.Data.Helpers
{
    public class DbMappings : Profile
    {
        public DbMappings()
        {
            CreateMap<Note, NoteEntity>().ReverseMap();
            CreateMap<Sport, SportEntity>().ReverseMap();
            CreateMap<SourceSport, SourceSportEntity>().ReverseMap();
            CreateMap<Country, CountryEntity>().ReverseMap();
            CreateMap<SourceTeam, SourceTeamEntity>().ReverseMap();
            CreateMap<Team, TeamEntity>().ReverseMap();
            CreateMap<League, LeagueEntity>().ReverseMap();
            CreateMap<SourceLeague, SourceLeagueEntity>().ReverseMap();

            CreateMap<Event, EventEntity>().ReverseMap();
            CreateMap<LineEntity, LineEntityEntity>().ReverseMap();
            CreateMap<MarketType, MarketTypeEntity>().ReverseMap();
            CreateMap<GamePeriod, GamePeriodEntity>().ReverseMap();
            CreateMap<Market, MarketType>().ReverseMap();
            CreateMap<Odd, OddEntity>().ReverseMap();

        }
    }
}
