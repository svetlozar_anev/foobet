﻿using System.Runtime.Serialization;

namespace fooBet.Core.Enums
{
    public enum Gender
    {
       // [EnumMember(Value = "Male")]
        Male = 1,
       // [EnumMember(Value = "Female")]
        Female = 2,
      //  [EnumMember(Value = "Mixed")]
        Mixed = 3,
      //  [EnumMember(Value = "Undefined")]
        Undefined = 4
    }
}
