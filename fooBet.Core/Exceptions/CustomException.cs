﻿using System.Net;
using System.Runtime.Serialization;

namespace fooBet.Core.Exceptions
{
    [Serializable]
    public class CustomException : Exception
    {
        public List<string> ErrorMessages { get; }

        public HttpStatusCode StatusCode { get; }

        public CustomException(string message, List<string> errors = default, HttpStatusCode statusCode = HttpStatusCode.InternalServerError)
            : base(message)
        {
            ErrorMessages = errors;
            StatusCode = statusCode;
        }

        protected CustomException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
