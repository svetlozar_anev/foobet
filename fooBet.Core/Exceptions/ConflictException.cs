﻿using System.Net;
using System.Runtime.Serialization;

namespace fooBet.Core.Exceptions
{
    [Serializable]
    public class ConflictException : CustomException
    {
        public ConflictException(string message)
            : base(message, null, HttpStatusCode.Conflict) { }

        protected ConflictException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
