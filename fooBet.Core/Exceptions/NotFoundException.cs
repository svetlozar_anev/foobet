﻿using System.Net;
using System.Runtime.Serialization;

namespace fooBet.Core.Exceptions
{
    [Serializable]
    public class NotFoundException : CustomException
    {
        public NotFoundException(string message) 
            : base(message, null, HttpStatusCode.NotFound) { }

        protected NotFoundException(SerializationInfo info, StreamingContext context)
             : base(info, context) { }
    }
}
