﻿using System.Net;
using System.Runtime.Serialization;

namespace fooBet.Core.Exceptions
{
    [Serializable]
    public class UnauthorizedAccessException : CustomException
    {

        public UnauthorizedAccessException(string message) 
            : base(message, null, HttpStatusCode.Unauthorized) { }

        protected UnauthorizedAccessException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
