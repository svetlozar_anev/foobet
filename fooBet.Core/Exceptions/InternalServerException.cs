﻿using System.Net;
using System.Runtime.Serialization;

namespace fooBet.Core.Exceptions
{
    [Serializable]
    public class InternalServerException : CustomException
    {
        public InternalServerException(string message) 
            : base(message, null, HttpStatusCode.InternalServerError) { }

        protected InternalServerException(SerializationInfo info, StreamingContext context) 
            : base(info, context) { }
    }
}
