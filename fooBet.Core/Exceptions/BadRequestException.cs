﻿using System.Net;
using System.Runtime.Serialization;

namespace fooBet.Core.Exceptions
{
    [Serializable]
    public class BadRequestException : CustomException
    {
        public BadRequestException(string message)
            : base(message, null, HttpStatusCode.Conflict) { }

        protected BadRequestException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
