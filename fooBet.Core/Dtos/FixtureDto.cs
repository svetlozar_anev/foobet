﻿namespace fooBet.Core.Dtos
{
    public class FixtureDto
    {
        public string DateRaw { get; set; }
        public string Team1 { get; set; }
        public string Team2 { get; set; }
    }
}
