﻿using System.Threading.Tasks;

namespace fooBet.Core.Contracts.Repos
{
    public interface IBaseRepository<TModel>
    {
        Task<TModel> AddAsync(TModel model);

        Task<IEnumerable<TModel>> GetAllAsync();
        
        Task<TModel> GetByIdAsync(int id);

        Task<TModel> GetByTitleAsync(string title);

        Task<TModel> UpdateAsync(TModel model);

        Task<bool> DeleteAsync(int id);
    }
}
