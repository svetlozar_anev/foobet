﻿using fooBet.Core.Models;

namespace fooBet.Core.Contracts.Repos
{
    public interface IGamePeriodRepository : IBaseRepository<GamePeriod>
    {
    }
}
