﻿using fooBet.Core.Models;

namespace fooBet.Core.Contracts.Repos
{
    public interface IEventRepository : IBaseRepository<Event>
    {
    }
}
