﻿using fooBet.Core.Models;

namespace fooBet.Core.Contracts.Repos
{
    public interface IMarketTypeRepository : IBaseRepository<MarketType> 
    {
    }
}
