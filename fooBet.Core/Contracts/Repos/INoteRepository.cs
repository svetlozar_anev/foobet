﻿using fooBet.Core.Models;

namespace fooBet.Core.Contracts.Repos
{
    public interface INoteRepository : IBaseRepository<Note>
    {
    }
}
