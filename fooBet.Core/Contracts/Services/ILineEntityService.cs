﻿using fooBet.Core.Models;

namespace fooBet.Core.Contracts.Services
{

        public interface ILineEntityService : IBaseService<LineEntity>
        {
            Task<IEnumerable<LineEntity>> NomenclatureSeedAsync(IEnumerable<LineEntity> lineEntities);
        }
}
