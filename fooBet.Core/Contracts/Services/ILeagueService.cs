﻿using fooBet.Core.Models;

namespace fooBet.Core.Contracts.Services
{
    public interface ILeagueService : IBaseService<League>
    {
        Task<IEnumerable<League>> NomenclatureSeed(IEnumerable<League> leagues);
    }
}
