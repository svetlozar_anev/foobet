﻿using fooBet.Core.Models;

namespace fooBet.Core.Contracts.Services
{
    public interface ISportService : IBaseService<Sport>
    {
        //TODO: Task<bool>
        Task<IEnumerable<Sport>> NomenclatureSeed(IEnumerable<Sport> sports);
    }
}
