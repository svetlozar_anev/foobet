﻿namespace fooBet.Core.Contracts.Services
{
    public interface IScraperService
    {
        Task GetFixturesAsync();
    }
}
