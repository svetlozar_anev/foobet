﻿using fooBet.Core.Models;

namespace fooBet.Core.Contracts.Services
{
    public interface IMarketTypeService : IBaseService<MarketType>
    {
        Task<IEnumerable<MarketType>> NomenclatureSeedAsync(IEnumerable<MarketType> marketTypes);
    }
}
