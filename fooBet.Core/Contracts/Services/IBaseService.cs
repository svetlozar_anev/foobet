﻿namespace fooBet.Core.Contracts.Services
{
    public interface IBaseService<TModel>
    {
        Task<TModel> AddAsync(TModel model);

        Task<IEnumerable<TModel>> GetAllAsync();

        Task<TModel> GetByIdAsync(int id);

        Task<TModel> GetByTitleAsync(string title);

        Task<TModel> UpdateAsync(int id, TModel model);

        Task<bool> DeleteAsync(int id);
    }
}
