﻿using fooBet.Core.Models;

namespace fooBet.Core.Contracts.Services
{
    public interface ITeamService : IBaseService<Team>
    {
        Task<IEnumerable<Team>> NomenclatureSeedAsync(IEnumerable<Team> teams);
    }
}
