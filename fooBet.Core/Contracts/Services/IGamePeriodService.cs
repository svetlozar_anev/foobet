﻿using fooBet.Core.Models;

namespace fooBet.Core.Contracts.Services
{
    public interface IGamePeriodService : IBaseService<GamePeriod>
    {
        Task<IEnumerable<GamePeriod>> NomenclatureSeedAsync(IEnumerable<GamePeriod> gamePeriods);
    }
}
