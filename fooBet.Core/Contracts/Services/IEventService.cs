﻿using fooBet.Core.Models;

namespace fooBet.Core.Contracts.Services
{
    public interface IEventService : IBaseService<Event>
    {
    }
}
