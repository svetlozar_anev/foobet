﻿using fooBet.Core.Models;

namespace fooBet.Core.Contracts.Services
{
    public interface INoteService : IBaseService<Note>
    {
    }
}
    