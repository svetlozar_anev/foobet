﻿using fooBet.Core.Models;

namespace fooBet.Core.Contracts.Services
{
    public interface ICountryService : IBaseService<Country>
    {
        Task<IEnumerable<Country>> NomenclatureSeed(IEnumerable<Country> countries);
    }
}
