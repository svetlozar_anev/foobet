﻿using fooBet.Core.Dtos;

namespace fooBet.Core.Contracts.Scraper
{
    public interface IUpcomingGamesScraper
    {
        Task<IEnumerable<FixtureDto>> GetFixturesAsync();
    }
}
