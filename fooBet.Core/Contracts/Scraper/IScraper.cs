﻿using fooBet.Core.Dtos;

namespace fooBet.Core.Contracts.Scraper
{
    public interface IScraper
    {
        Task <IEnumerable<FixtureDto>>GetFixturesAsync();
    }
}
