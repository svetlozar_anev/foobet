﻿using fooBet.Core.Models.Abstract;

namespace fooBet.Core.Models
{
    public class Sport : BaseTitleModel
    {
        public ICollection<SourceSport> SourceSports { get; set; }
    }
}
