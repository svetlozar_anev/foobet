﻿namespace fooBet.Core.Models.Abstract
{
    public abstract class BaseIdModel
    {
        public int Id { get; set; }
    }
}
