﻿namespace fooBet.Core.Models.Abstract
{
    public abstract class BaseTitleModel : BaseIdModel
    {
        public string Title { get; set; }
    }
}
