﻿namespace fooBet.Core.Models
{
    public class Odd
    {
        public Market Market { get; set; }
        public string OutcomeKey { get; set; } //O/U... !X2
        public double Spread { get; set; }
        public double Value { get; set; }
    }
}
