﻿using fooBet.Core.Models.Abstract;

namespace fooBet.Core.Models
{
    public class MarketType : BaseTitleModel
    {
        public bool HasSpread { get; set; }
        public string ShortTitle { get; set; }
        public List<string> PossibleOutcomes { get; set; }
    }
}
