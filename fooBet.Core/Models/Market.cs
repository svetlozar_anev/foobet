﻿using fooBet.Core.Models.Abstract;

namespace fooBet.Core.Models
{
    public class Market : BaseIdModel
    {
        public Event Event { get; set; }
        public GamePeriod GamePeriod { get; set; }
        public LineEntity LineEntity { get; set; }
        public MarketType MarketType { get; set; }
        public int OptionsIndex { get; set; }
    }
}
