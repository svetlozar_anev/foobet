﻿using fooBet.Core.Enums;
using fooBet.Core.Models.Abstract;

namespace fooBet.Core.Models
{
    public class Team : BaseTitleModel
    {
        public Sport Sport { get; set; }
        public Country Country { get; set; }
        public Gender Gender { get; set; }
        public ICollection<SourceTeam> SourceTeams { get; set; }
    }
}
