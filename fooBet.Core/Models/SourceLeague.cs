﻿using fooBet.Core.Models.Abstract;

namespace fooBet.Core.Models
{
    public class SourceLeague : BaseTitleModel
    {
        public int LeagueId { get; set; }
    }
}
