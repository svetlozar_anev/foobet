﻿using fooBet.Core.Enums;
using fooBet.Core.Models.Abstract;

namespace fooBet.Core.Models
{
    public class League : BaseTitleModel
    {
        public Sport Sport { get; set; }
        public Gender Gender { get; set; }
        public ICollection<SourceLeague> SourceLeagues { get; set; }
    }
}