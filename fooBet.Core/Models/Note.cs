﻿using fooBet.Core.Models.Abstract;

namespace fooBet.Core.Models
{
    public class Note : BaseTitleModel
    {
        public string Message { get; set; }
    }
}
