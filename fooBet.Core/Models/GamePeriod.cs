﻿using fooBet.Core.Models.Abstract;

namespace fooBet.Core.Models
{
    public class GamePeriod : BaseTitleModel
    {
        public Sport Sport { get; set; }
    }
}
