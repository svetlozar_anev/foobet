﻿using fooBet.Core.Models.Abstract;

namespace fooBet.Core.Models
{
    public class Event : BaseTitleModel
    {
        public League League { get; set; }
        public Team HomeTeam { get; set; }
        public Team AwayTeam { get; set; }
        public DateTime StartDate { get; set; }
    }
}
