﻿namespace fooBet.Api.ViewModels
{
    public class NoteInputModel
    {
        public string Title { get; set; }

        public string Message { get; set; }
    }
}
