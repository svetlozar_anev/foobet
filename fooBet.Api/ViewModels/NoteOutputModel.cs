﻿namespace fooBet.Api.ViewModels
{
    public class NoteOutputModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Message { get; set; }
    }
}
