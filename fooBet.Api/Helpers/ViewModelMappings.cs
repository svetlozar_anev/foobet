﻿using AutoMapper;
using fooBet.Api.ViewModels;
using fooBet.Core.Models;

namespace fooBet.Api.Helpers
{
    public class ViewModelMappings : Profile
    {
        public ViewModelMappings()
        {
            CreateMap<Note, NoteOutputModel>();
            CreateMap<NoteInputModel, Note>();
        }
    }
}
