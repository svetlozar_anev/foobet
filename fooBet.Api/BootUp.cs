﻿using fooBet.Api.Helpers;
using fooBet.Data;
using fooBet.Data.Helpers;
using fooBet.Scraper.Helpers;
using fooBet.Services.Helpers;
using fooBet.Services.Middleware;
using Microsoft.EntityFrameworkCore;

namespace fooBet.Api
{
    public class BootUp
    {
        public static IConfiguration Configuration { get; set; }
        protected BootUp(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public static WebApplication MakeRun(string[] args)
        {
            //uncomment to (re)migrate db 
            // ResetDb();

            var builder = WebApplication.CreateBuilder(args); 
            ConfigureServices(builder.Services);
            var app = builder.Build();
            Configure(app);
            return app;
        }

        public static void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();

            services.AddDbContext<DataContext>(options =>
             options.UseNpgsql(Configuration.GetConnectionString("DataContext")));
            services.AddMvc(opt => {
                opt.SuppressAsyncSuffixInActionNames = false;
            });

            services.AddAutoMapper(typeof(ViewModelMappings).Assembly);
            services.AddAutoMapper(typeof(DbMappings).Assembly);

            services.RegisterRepositories();
            services.RegisterServices();
            services.RegisterScraper();
        }

        private static void Configure(WebApplication app)
        {
            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();
         //   app.UseMiddleware<ExceptionMiddleware>();

            app.UseAuthorization();
            app.MapControllers();

        }

        private static void ResetDb()
        {
            var optionsBuilder = new DbContextOptionsBuilder<DataContext>();
            optionsBuilder.UseNpgsql(Configuration.GetConnectionString("DataContext"));

            var db = new DataContext(optionsBuilder.Options);
            db.Database.EnsureDeleted();
            db.Database.Migrate();
        }

    }
}
