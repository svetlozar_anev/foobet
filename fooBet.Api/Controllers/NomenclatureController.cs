﻿using fooBet.Core.Contracts.Services;
using Microsoft.AspNetCore.Mvc;

namespace fooBet.Api.Controllers
{
    [ApiController]
    [Route("api/nomenclature")]
    public class NomenclatureController : ControllerBase
    {
        private readonly INomenclatureService _service;

        public NomenclatureController(INomenclatureService service)
        {
            _service= service;
        }

        [HttpPost]
        [Route("seed")]
        public async Task<IActionResult> SeedAsync()
        {
            await _service.SeedAsync();
            return Ok();
        }
    }
}
