﻿using AutoMapper;
using fooBet.Api.ViewModels;
using fooBet.Core.Contracts.Services;
using fooBet.Core.Models;
using Microsoft.AspNetCore.Mvc;

namespace fooBet.Api.Controllers
{
    [ApiController]
    [Route("api/note")]
    public class NoteController : ControllerBase
    {
        private readonly INoteService _service;
        private readonly IMapper _mapper;

        public NoteController(INoteService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> AddAsync(NoteInputModel viewModel)
        {
            var input = _mapper.Map<Note>(viewModel);
            var result = _mapper.Map<NoteOutputModel>(await _service.AddAsync(input));

            return CreatedAtAction(nameof(GetByIdAsync), new { id = result.Id }, result);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            var viewModels = _mapper.Map<IEnumerable<NoteOutputModel>>(await _service.GetAllAsync());
            return Ok(viewModels);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var viewModel = _mapper.Map<NoteOutputModel>(await _service.GetByIdAsync(id));
            return Ok(viewModel);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync(int id, NoteInputModel viewModel)
        { 
            var note = _mapper.Map<Note>(viewModel);
            return Ok(_mapper.Map<NoteOutputModel>(await _service.UpdateAsync(id, note)));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var result = await _service.DeleteAsync(id);
            if (!result)
            {
                return NotFound();
            }

            return Ok();
        }
    }
}
