﻿using fooBet.Core.Contracts.Services;
using Microsoft.AspNetCore.Mvc;

namespace fooBet.Api.Controllers
{
    [ApiController]
    [Route("api/scraper")]
    public class ScraperController : ControllerBase
    {
        private readonly IScraperService _service;

        public ScraperController(IScraperService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("fixtures")]
        public async Task<IActionResult> Index()
        {
            await _service.GetFixturesAsync();

            return Ok();
        }
    }
}
