using Microsoft.EntityFrameworkCore;

namespace fooBet.Api
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var app = BootUp.MakeRun(args);
            app.Run();
        }
    }
}