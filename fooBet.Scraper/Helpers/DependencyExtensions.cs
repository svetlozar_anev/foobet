﻿using fooBet.Core.Contracts.Scraper;
using Microsoft.Extensions.DependencyInjection;

namespace fooBet.Scraper.Helpers
{
    public static class DependencyExtensions
    {
        public static IServiceCollection RegisterScraper(this IServiceCollection services)
        {
            services.AddScoped<IUpcomingGamesScraper, UpcomingGamesScraper>();

            return services;
        }
    }
}
