﻿using fooBet.Core.Contracts.Scraper;
using fooBet.Core.Dtos;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace fooBet.Scraper
{
    public class UpcomingGamesScraper : IUpcomingGamesScraper
    {
        // private const string crawlFixturesUrl = "https://www.flashscore.com/football/england/premier-league/fixtures/";
        // 
        //

        private const string crawlFixturesUrl = "https://www.flashscore.com/football/ireland/premier-division/fixtures/";

        private readonly IWebDriver _driver;

        public UpcomingGamesScraper()
        {
            var options = new ChromeOptions();
            var arguments = new List<string>
            {
                "enable-automation",
                "excludeSwitches",
                "enable-logging",
                "disable-popup-blocking"
            };
            options.AddExcludedArguments(arguments);

            _driver = new ChromeDriver(options);
        }

        public async Task<IEnumerable<FixtureDto>> GetFixturesAsync()
        {
            _driver.Navigate().GoToUrl(crawlFixturesUrl);
            Thread.Sleep(5000);

             var acceptButton = _driver.FindElement(By.XPath("//button[contains(text(), 'Accept')]"));
          //  var acceptButton = FindMany(By.XPath("//button[contains(text(), 'Accept')]"));
            Thread.Sleep(5000);

            acceptButton?.Click();
            Thread.Sleep(5000);

            var rawFixtureList = new List<FixtureDto>();
        //    IReadOnlyCollection<IWebElement> matches = _driver.FindElements(By.XPath("//*[starts-with(@title, 'Click for match')]"));
            IReadOnlyCollection<IWebElement> matches = FindMany(By.XPath("//*[starts-with(@title, 'Click for match')]"));

            foreach (var match in matches)
            {
                var fixtureRawArr = match.Text.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);

                rawFixtureList.Add(new FixtureDto
                {
                    DateRaw = fixtureRawArr[0],
                    Team1 = fixtureRawArr[1],
                    Team2 = fixtureRawArr[2]
                });
            }

            return await Task.FromResult<IEnumerable<FixtureDto>>(rawFixtureList);
        }

        //public async Task<List<string>> GetOddsAsync() 
        //{
        //    return awnew 
        //}

        //private IReadOnlyCollection<IWebElement> FindOne(By by)
        //{
        //    Stopwatch stopwatch = Stopwatch.StartNew();
        //    while (stopwatch.ElapsedMilliseconds < 10 * 1000)
        //    {
        //        var element = _driver.FindElement(by);
        //        if (element.)
        //            return elements;
        //        Thread.Sleep(10);
        //    }
        //    return new ReadOnlyCollection<IWebElement>(new List<IWebElement>());
        //}

        private IReadOnlyCollection<IWebElement> FindMany(By by)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            while (stopwatch.ElapsedMilliseconds < 10 * 1000)
            {
                var elements = _driver.FindElements(by);
                if (elements.Count > 0)
                    return elements;
                Thread.Sleep(10);
            }
            return new ReadOnlyCollection<IWebElement>(new List<IWebElement>());
        }

    }
}
