﻿using fooBet.Core.Contracts.Services;
using fooBet.Core.Exceptions;
using fooBet.Core.Models;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace fooBet.Services
{
    public class NomenclatureService : INomenclatureService
    {
        private readonly ISportService _sportService;
        private readonly ILeagueService _leagueService;
        private readonly ICountryService _countryService;
        private readonly ITeamService _teamService;
        private readonly ILineEntityService _lineEntityService;
        private readonly IGamePeriodService _gamePeriodService;
        private readonly IMarketTypeService _marketTypeService;


        public NomenclatureService(ISportService sportService,
                                   ILeagueService leagueService,
                                   ICountryService countryService,
                                   ITeamService teamService,
                                   ILineEntityService lineEntityService,
                                   IGamePeriodService gamePeriodService,
                                   IMarketTypeService marketTypeService)
        {
            _sportService = sportService;
            _leagueService = leagueService;
            _countryService = countryService;
            _teamService = teamService;
            _lineEntityService = lineEntityService;
            _gamePeriodService = gamePeriodService;
            _marketTypeService = marketTypeService;
        }

        //todo: remove breakpoint lines, make generic method for seeding
        public async Task SeedAsync()
        {
            var sportsPath = @"..\fooBet.Core\Nomenclature\sports.json";
            var jsonSports = File.ReadAllText(sportsPath);
            ;
            if (jsonSports != null)
            {
                var sports = JsonSerializer.Deserialize<List<Sport>>(jsonSports);
                await _sportService.NomenclatureSeed(sports);
            }
            else
            {
                throw new InternalServerException("Json read text error.");
            }


            var leaguesPath = @"..\fooBet.Core\Nomenclature\leagues.json";
            var jsonLeagues = File.ReadAllText(leaguesPath);
            if (jsonLeagues != null)
            {
                var options = new JsonSerializerOptions();
                options.Converters.Add(new JsonStringEnumConverter());
                var leagues = JsonSerializer.Deserialize<List<League>>(jsonLeagues, options);
                await _leagueService.NomenclatureSeed(leagues);
            }
            else
            {
                throw new InternalServerException("Json read text error.");
            }


            var countriesPath = @"..\fooBet.Core\Nomenclature\countries.json";
            var jsonCountries = File.ReadAllText(countriesPath);
            if (jsonCountries != null)
            {
                var countries = JsonSerializer.Deserialize<List<Country>>(jsonCountries);
                await _countryService.NomenclatureSeed(countries);
            }
            else
            {
                throw new InternalServerException("Json read text error.");
            }


            var teamsPath = @"..\fooBet.Core\Nomenclature\teams.json";
            var jsonTeams = File.ReadAllText(teamsPath);
            if (jsonTeams != null)
            {
                var options = new JsonSerializerOptions();
                options.Converters.Add(new JsonStringEnumConverter());
                var teams = JsonSerializer.Deserialize<List<Team>>(jsonTeams, options);
                ;
                await _teamService.NomenclatureSeedAsync(teams);
            }
            else
            {
                throw new InternalServerException("Json read text error.");
            }

            var lineEntitiesPath = @"..\fooBet.Core\Nomenclature\lineEntities.json";
            var jsonLineEntities = File.ReadAllText(lineEntitiesPath);
            if (jsonLineEntities != null)
            {
                var lineEntities = JsonSerializer.Deserialize<List<LineEntity>>(jsonLineEntities);
                ;
                await _lineEntityService.NomenclatureSeedAsync(lineEntities);
            }
            else
            {
                throw new InternalServerException("Json read text error.");
            }

                        ;
            var gamePeriodsPath = @"..\fooBet.Core\Nomenclature\gamePeriods.json";
            var jsonGamePeriods = File.ReadAllText(gamePeriodsPath);
            if (jsonGamePeriods != null)
            {
                var gamePeriods = JsonSerializer.Deserialize<List<GamePeriod>>(jsonGamePeriods);
                await _gamePeriodService.NomenclatureSeedAsync(gamePeriods);
            }
            else
            {
                throw new InternalServerException("Json read text error.");
            }

            var marketTypesPath = @"..\fooBet.Core\Nomenclature\marketTypes.json";
            var jsonMarketTypes = File.ReadAllText(marketTypesPath);
            if (jsonMarketTypes != null)
            {
                var marketTypes = JsonSerializer.Deserialize<List<MarketType>>(jsonMarketTypes);
                await _marketTypeService.NomenclatureSeedAsync(marketTypes);
            }
            else
            {
                throw new InternalServerException("Json read text error.");
            }
        }
    }
}

//TODO : nomenclature seed for sourceSports, sourceLeagues
