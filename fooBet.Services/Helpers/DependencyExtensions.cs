﻿using fooBet.Core.Contracts.Services;
using fooBet.Services.Middleware;
using Microsoft.Extensions.DependencyInjection;

namespace fooBet.Services.Helpers
{
    public static class DependencyExtensions
    {
        public static IServiceCollection RegisterServices(this IServiceCollection services)
        {
         //   services.AddTransient<ExceptionMiddleware>();

            services.AddScoped<INoteService, NoteService>();
            services.AddScoped<ISportService, SportService>();
            services.AddScoped<ILeagueService, LeagueService>();
            services.AddScoped<ITeamService, TeamService>();
            services.AddScoped<ICountryService, CountryService>();
            services.AddScoped<INomenclatureService, NomenclatureService>();
            services.AddScoped<IScraperService, ScraperService>();
            services.AddScoped<IEventService, EventService>();
            services.AddScoped<ILineEntityService, LineEntityService>();
            services.AddScoped<IGamePeriodService, GamePeriodService>();
            services.AddScoped<IMarketTypeService, MarketTypeService>();

            return services;
        }
    }
}
