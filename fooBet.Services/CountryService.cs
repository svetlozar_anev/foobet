﻿using fooBet.Core.Contracts.Repos;
using fooBet.Core.Contracts.Services;
using fooBet.Core.Exceptions;
using fooBet.Core.Models;

namespace fooBet.Services
{
    public class CountryService : BaseService<Country, ICountryRepository>, ICountryService
    {
        private readonly ICountryRepository _repository;
        public CountryService(ICountryRepository repo) : base(repo)
        {
            _repository = repo;
        }

        public async Task<IEnumerable<Country>> NomenclatureSeed(IEnumerable<Country> countries)
        {
            if (_repository.GetAllAsync().Result.ToList().Count == 0)
            {
                foreach (var country in countries)
                {
                    await _repository.AddAsync(country);
                }
            }
            else 
            {
                throw new ConflictException("Countries already seeded.");
            }

            return await _repository.GetAllAsync();
        }
    }
}
