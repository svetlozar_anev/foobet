﻿using fooBet.Core.Contracts.Scraper;
using fooBet.Core.Contracts.Services;
using fooBet.Core.Exceptions;
using fooBet.Core.Models;

namespace fooBet.Services
{
    public class ScraperService : IScraperService
    {
        private readonly IUpcomingGamesScraper _scraper;
        private readonly ITeamService _teamService;
        private readonly ILeagueService _leagueService;
        private readonly IEventService _eventService;

        public ScraperService(IUpcomingGamesScraper scraper,
                              ITeamService teamService,
                              ILeagueService leagueService,
                              IEventService eventService)
        {
            _scraper = scraper;
            _teamService = teamService;
            _leagueService = leagueService;
            _eventService = eventService;
        }

        public async Task GetFixturesAsync()
        {
            var fixtures = await _scraper.GetFixturesAsync();

            foreach (var fixture in fixtures)
            {
                var rawDateSplit = fixture.DateRaw.Split(new char[] { ' ', ':', '.' }, StringSplitOptions.RemoveEmptyEntries);

                var title = string.Concat(fixture.DateRaw, " - ", fixture.Team1, " vs. ", fixture.Team2);

                var existingEvent = await _eventService.GetByTitleAsync(title);

                if (existingEvent == null)
                {
                    var date = int.Parse(rawDateSplit[0]);
                    var month = int.Parse(rawDateSplit[1]);
                    var hour = int.Parse(rawDateSplit[2]);
                    var minute = int.Parse(rawDateSplit[3]);
                    var year = 2023;
                    var seconds = 0;
                    ;

                    var newEvent = new Event
                    {
                       // League = await _leagueService.GetByTitleAsync("Premier League"),
                        League = await _leagueService.GetByTitleAsync("Premier Division"),
                        Title = title,
                        HomeTeam = await _teamService.GetByTitleAsync(fixture.Team1),
                        AwayTeam = await _teamService.GetByTitleAsync(fixture.Team2),
                        StartDate = new DateTime(year, month, date, hour, minute, seconds).ToUniversalTime()
                    };

                    ;
                    var res = await _eventService.AddAsync(newEvent);
                }
                else 
                {
                    //logger here, not exception (???)
                    throw new BadRequestException($"{title} already added.");                
                }
            }


            //throw new NotImplementedException();
        }
    }
}
