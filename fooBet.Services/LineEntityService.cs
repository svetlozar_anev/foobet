﻿using fooBet.Core.Contracts.Repos;
using fooBet.Core.Contracts.Services;
using fooBet.Core.Exceptions;
using fooBet.Core.Models;

namespace fooBet.Services
{
    public class LineEntityService : BaseService<LineEntity, ILineEntityRepository>, ILineEntityService
    {
        private readonly ILineEntityRepository _repository;

        public LineEntityService(ILineEntityRepository repo) : base(repo)
        {
            _repository = repo;
        }

        public async Task<IEnumerable<LineEntity>> NomenclatureSeedAsync(IEnumerable<LineEntity> lineEntities)
        {
            if (_repository.GetAllAsync().Result.ToList().Count == 0)
            {
                foreach (var lineEntity in lineEntities)
                {
                    await _repository.AddAsync(lineEntity);
                }
            }
            else
            {
                throw new ConflictException("Line Entities already seeded.");
            }

            return await _repository.GetAllAsync();

        }
    }
}
