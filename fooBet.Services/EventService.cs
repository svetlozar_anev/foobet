﻿using fooBet.Core.Contracts.Repos;
using fooBet.Core.Contracts.Services;
using fooBet.Core.Models;

namespace fooBet.Services
{
    public class EventService : BaseService<Event, IEventRepository>, IEventService
    {
        public EventService(IEventRepository repo) : base(repo)
        {
        }
    }
}
