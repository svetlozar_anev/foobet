﻿using fooBet.Core.Contracts.Repos;
using fooBet.Core.Contracts.Services;
using fooBet.Core.Exceptions;
using fooBet.Core.Models;

namespace fooBet.Services
{
    public class MarketTypeService : BaseService<MarketType, IMarketTypeRepository>, IMarketTypeService
    {
        private readonly IMarketTypeRepository _repository;
        public MarketTypeService(IMarketTypeRepository repo) : base(repo)
        {
            _repository = repo;
        }

        public async Task<IEnumerable<MarketType>> NomenclatureSeedAsync(IEnumerable<MarketType> marketTypes)
        {
            if (_repository.GetAllAsync().Result.ToList().Count == 0)
            {
                foreach (var marketType in marketTypes)
                {
                    await _repository.AddAsync(marketType);
                }
            }
            else
            {
                throw new ConflictException("Market Types already seeded.");
            }

            return await _repository.GetAllAsync();
        }
    }
}
