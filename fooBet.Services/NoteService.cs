﻿using fooBet.Core.Contracts.Repos;
using fooBet.Core.Contracts.Services;
using fooBet.Core.Models;

namespace fooBet.Services
{
    public class NoteService : BaseService<Note, INoteRepository>, INoteService
    {
        public NoteService(INoteRepository repo) : base(repo)
        {
        }

    }
}
