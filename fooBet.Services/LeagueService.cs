﻿using fooBet.Core.Contracts.Repos;
using fooBet.Core.Contracts.Services;
using fooBet.Core.Exceptions;
using fooBet.Core.Models;

namespace fooBet.Services
{
    public class LeagueService : BaseService<League, ILeagueRepository>, ILeagueService
    {
        private readonly ILeagueRepository _repository;
        public LeagueService(ILeagueRepository repo) : base(repo)
        {
            _repository = repo;
        }

        public async Task<IEnumerable<League>> NomenclatureSeed(IEnumerable<League> leagues)
        {
            if (_repository.GetAllAsync().Result.ToList().Count == 0)
            {
                foreach (var league in leagues)
                {
                    await _repository.AddAsync(league);
                }
            }
            else
            {
                throw new ConflictException("Leagues already seeded.");
            }

            return await _repository.GetAllAsync();

        }
    }
}
