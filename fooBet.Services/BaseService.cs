﻿using fooBet.Core.Contracts.Repos;
using fooBet.Core.Contracts.Services;
using fooBet.Core.Exceptions;
using fooBet.Core.Models.Abstract;

namespace fooBet.Services
{
    public abstract class BaseService<TModel, TRepository> : IBaseService<TModel>
     where TModel : BaseTitleModel
     where TRepository : IBaseRepository<TModel>
    {

        private readonly TRepository _repo;

        protected BaseService(TRepository repo)
        {
            _repo = repo;
        }

        public async Task<TModel> AddAsync(TModel model)
        {
            return await _repo.AddAsync(model);
        }

        public async Task<IEnumerable<TModel>> GetAllAsync()
        {
            return await _repo.GetAllAsync();
        }

        public async Task<TModel> GetByIdAsync(int id)
        {
            return await _repo.GetByIdAsync(id);
        }

        public async Task<TModel> GetByTitleAsync(string title)
        {
            return await _repo.GetByTitleAsync(title);
        }


        public async Task<TModel> UpdateAsync(int id, TModel model)
        {
            var entity = await _repo.GetByIdAsync(id);
            if (entity == null)
            {
                throw new NotFoundException("Entity not found.");
            }


            model.Id = id;


            return await _repo.UpdateAsync(model);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            return await _repo.DeleteAsync(id);
        }
    }
}
