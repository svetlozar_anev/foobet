﻿using fooBet.Core.Contracts.Repos;
using fooBet.Core.Contracts.Services;
using fooBet.Core.Exceptions;
using fooBet.Core.Models;

namespace fooBet.Services
{
    public class TeamService : BaseService<Team, ITeamRepository>, ITeamService
    {
        private readonly ITeamRepository _repository;
        public TeamService(ITeamRepository repo) : base(repo)
        {
            _repository = repo;
        }

        public async Task<IEnumerable<Team>> NomenclatureSeedAsync(IEnumerable<Team> teams)
        {
            if (_repository.GetAllAsync().Result.ToList().Count == 0)
            {
                foreach (var team in teams)
                {
                    await _repository.AddAsync(team);
                }
            }
            else
            {
                throw new ConflictException("Teams already seeded.");
            }

            return await _repository.GetAllAsync();
        }


    }
}
