﻿using fooBet.Core.Contracts.Repos;
using fooBet.Core.Contracts.Services;
using fooBet.Core.Exceptions;
using fooBet.Core.Models;

namespace fooBet.Services
{
    public class GamePeriodService : BaseService<GamePeriod, IGamePeriodRepository>, IGamePeriodService
    {
        private readonly IGamePeriodRepository _repository;

        public GamePeriodService(IGamePeriodRepository repo) : base(repo)
        {
            _repository = repo;
        }


        public async Task<IEnumerable<GamePeriod>> NomenclatureSeedAsync(IEnumerable<GamePeriod> gamePeriods)
        {
            if (_repository.GetAllAsync().Result.ToList().Count == 0)
            {
                foreach (var gamePeriod in gamePeriods)
                {
                    await _repository.AddAsync(gamePeriod);
                }
            }
            else
            {
                throw new ConflictException("Game Periods already seeded.");
            }

            return await _repository.GetAllAsync();
        }
    }
}
