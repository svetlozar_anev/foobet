﻿using fooBet.Core.Contracts.Repos;
using fooBet.Core.Contracts.Services;
using fooBet.Core.Exceptions;
using fooBet.Core.Models;

namespace fooBet.Services
{
    public class SportService : BaseService<Sport, ISportRepository>, ISportService
    {
        private readonly ISportRepository _repository;
        public SportService(ISportRepository repo) : base(repo)
        {
            _repository = repo;
        }

        public async Task<IEnumerable<Sport>> NomenclatureSeed(IEnumerable<Sport> sports)
        {
            if (_repository.GetAllAsync().Result.ToList().Count == 0)
            {
                foreach (var sport in sports)
                {
                    await _repository.AddAsync(sport);
                }
            }
            else
            {
                throw new ConflictException("Sports already seeded.");
            }

            return await _repository.GetAllAsync();
        }
    }
}
